const {ObjectID} = require('mongodb');

const {mongoose} = require('./../server/db/mongoose');
const {Todo} = require('./../server/models/todo');
const {User} = require('./../server/models/users');

// Todo.remove({}).then((result) => {
//     console.log(result);
// });

//Todo.findOneAndRemove
//Todo.findByIdAndRemove

// Todo.findOneAndRemove({_id: '5a9a8c078d83a390440e4424'}).then((result) => {
//     console.log(result);
// });

Todo.findByIdAndRemove('5a9a8c078d83a390440e4424').then((todo) => {
    console.log(todo);
});