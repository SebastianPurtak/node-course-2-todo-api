const {ObjectID} = require('mongodb');

const {mongoose} = require('./../server/db/mongoose');
const {Todo} = require('./../server/models/todo');
const {User} = require('./../server/models/users');

// var id = '5a995332447c29104dac802e11';

// if(!ObjectID.isValid(id)){
//     console.log('ID not valide');
// };


// Todo.find({
//     _id: id
// }).then((todos) => {
//     console.log('Todos', todos);
// });

// Todo.findOne({
//     _id: id
// }).then((todo) => {
//     console.log('Todo', todo);
// });

// Todo.findById(id).then((todo) => {
//     if(!todo){
//         return console.log('ID not found');
//     }
//     console.log('Todo by id', todo);
// }).catch((e) => console.log(e));

var userID = '5a982b620ff38c03926dd6d2';

User.findById(userID).then((user) => {
    if(!user){
        return console.log('User not found.');
    }
    console.log('User by id ', JSON.stringify(user, undefined, 2));
}).catch((e) => console.log(e));